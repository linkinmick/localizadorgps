package com.gps.dmf.frausto.localizadorgps;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.SupportMapFragment;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DashboardMapActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    SupportMapFragment mapFragment;

    public ImageView onIcon;
    public ImageView offIcon;
    public TextView connectionStatus;
    public String url= "http://gps.frausto.mx/api/logs/isOnline";
    public String statusUrl= "http://gps.frausto.mx/api/status/1";
    private OkHttpClient client;
    private AsyncTask<String, String, Boolean> runnableServerAvailability;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_map);

        Toolbar toolbar = findViewById(R.id.gps_app_toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        FloatingActionButton fab = findViewById(R.id.fab);

        //Initialize OkHttp client
        this.client = HttpOkClientSingleton.getInstance().getClient();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/

                Log.e("Click", "Trying to update");

                //Initialize OkHttp client
                OkHttpClient client = HttpOkClientSingleton.getInstance().getClient();

                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                RequestBody body = RequestBody.create(JSON, "{\"status\":\"1\"}");

                Request request = new Request.Builder()
                        .url(statusUrl)
                        .put(body) //PUT
                        .build();

                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.e("CallFail", e.toString());
                        call.cancel();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {

                        final String responseBody = response.body().string();

                        if (!response.isSuccessful()) {
                            throw new IOException("Error response " + response);
                        }

                        Log.i("Changed",responseBody);


                        /*MapsActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    setMarkerPosition(myResponse, marker);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });*/

                    }
                });
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(new MapsActivity());

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        onIcon = findViewById(R.id.online_icon);
        offIcon = findViewById(R.id.offline_icon);
        //connectionStatus = findViewById(R.id.connection_status);

        this.runnableServerAvailability = new RunnableServerAvailability(this);
        this.runnableServerAvailability.execute(this.url);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /*void run() throws IOException {

        Request request = new Request.Builder()
                .url(url)
                .build();

        //connectionStatus.setText("Verificando conexión con servidor...");

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String apiResponse = response.body().string();


                DashboardMapActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean response = Boolean.parseBoolean(apiResponse);
                        handleStatus(response);
                    }
                });


                Log.e("GetAPIDataActivity", apiResponse);
            }
        });
    }*/

    public void handleStatus(Boolean isOnline) {
        if(isOnline){
            onIcon.setVisibility(View.VISIBLE);
            offIcon.setVisibility(View.GONE);
        } else {
            offIcon.setVisibility(View.VISIBLE);
            onIcon.setVisibility(View.GONE);
            Toast.makeText(this, "Servidor no disponible", Toast.LENGTH_LONG).show();
        }
        //connectionStatus.setVisibility(View.VISIBLE);
    }
}
