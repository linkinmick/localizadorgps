package com.gps.dmf.frausto.localizadorgps;

import okhttp3.OkHttpClient;

/**
 * Created by miguel frausto on 3/2/18.
 */

public class HttpOkClientSingleton {
    private static HttpOkClientSingleton instance = null;
    private OkHttpClient client;

    private HttpOkClientSingleton(){
        //Singleton object
        this.client = new OkHttpClient();
    }

    /**
     * Initialize sigleton instance with Lazy loading
     *
     * @return OkHttpClient
     */
    public static HttpOkClientSingleton getInstance(){
        if(instance == null){
            instance = new HttpOkClientSingleton();
        }

        return instance;
    }

    public OkHttpClient getClient(){
        return this.client;
    }
}
