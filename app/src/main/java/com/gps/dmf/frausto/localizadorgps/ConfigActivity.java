package com.gps.dmf.frausto.localizadorgps;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

public class ConfigActivity extends AppCompatActivity {
    public Switch autoCenter;
    public Switch autoZoom;
    public Button saveConfig;

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        autoCenter = findViewById(R.id.sw_centrado_auto);
        autoZoom = findViewById(R.id.sw_auto_zoom);

        autoCenter.setOnClickListener(changeCenterListener);
        autoZoom.setOnClickListener(changeZoomListener);

        sharedPref = getSharedPreferences("configuraciones", Context.MODE_PRIVATE);
        editor = sharedPref.edit();

    }

    @Override
    protected void onResume() {
        super.onResume();

        autoZoom.setChecked(sharedPref.getBoolean("auto_zoom", true));
        autoCenter.setChecked(sharedPref.getBoolean("auto_center", true));
    }

    protected void closeActivity() {
        this.finish();
    }

    View.OnClickListener changeCenterListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            editor.putBoolean("auto_center", autoCenter.isChecked());
            editor.commit();
        }
    };

    View.OnClickListener changeZoomListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            editor.putBoolean("auto_zoom", autoZoom.isChecked());
            editor.commit();
        }
    };
}
