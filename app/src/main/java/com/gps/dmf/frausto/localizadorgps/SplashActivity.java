package com.gps.dmf.frausto.localizadorgps;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SplashActivity extends AppCompatActivity {

    public String url= "http://gps.frausto.mx/api/logs/isOnline";
    private OkHttpClient client;

    public LinearLayout btnLocate;
    public LinearLayout btnConfig;
    public ImageView onIcon;
    public ImageView offIcon;
    public TextView connectionStatus;

    boolean autoZoom;
    boolean autoCenter;

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Toolbar toolbar = (Toolbar) findViewById(R.id.gps_app_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        onIcon = findViewById(R.id.online_icon);
        offIcon = findViewById(R.id.offline_icon);
        connectionStatus = findViewById(R.id.connection_status);
        btnLocate = findViewById(R.id.locate_layout);
        btnConfig = findViewById(R.id.config_layout);
        btnLocate.setOnClickListener(locateLayoutListener);
        btnConfig.setOnClickListener(configLayoutListener);

        //Initialize OkHttp client
        this.client = HttpOkClientSingleton.getInstance().getClient();

        sharedPref = getSharedPreferences("configuraciones", Context.MODE_PRIVATE);

        try {
            run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    View.OnClickListener locateLayoutListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Llama sección de configuraciones
            Intent intent = new Intent(getApplicationContext(), DashboardMapActivity.class);
            startActivity(intent);
        }
    };

    View.OnClickListener configLayoutListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Llama sección de configuraciones
            Intent intent = new Intent(getApplicationContext(), ConfigActivity.class);
            startActivity(intent);
        }
    };

    void run() throws IOException {

        Request request = new Request.Builder()
                .url(url)
                .build();

        connectionStatus.setText("Verificando conexión con servidor...");

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String apiResponse = response.body().string();


                SplashActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean response = Boolean.parseBoolean(apiResponse);
                        handleStatus(response);
                    }
                });


                Log.e("GetAPIDataActivity", apiResponse);
            }
        });
    }

    public void handleStatus(Boolean isOnline) {
        if(isOnline){
            onIcon.setVisibility(View.VISIBLE);
            offIcon.setVisibility(View.GONE);
        } else {
            offIcon.setVisibility(View.VISIBLE);
            onIcon.setVisibility(View.GONE);
            Toast.makeText(this, "Servidor no disponible", Toast.LENGTH_LONG).show();
        }
        connectionStatus.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        autoCenter = sharedPref.getBoolean("auto_center", true);
        autoZoom = sharedPref.getBoolean("auto_zoom", true);
    }
}
