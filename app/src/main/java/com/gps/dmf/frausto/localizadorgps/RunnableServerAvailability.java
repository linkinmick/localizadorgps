package com.gps.dmf.frausto.localizadorgps;

import android.content.Context;
import android.os.AsyncTask;

import java.io.IOException;
import java.lang.ref.WeakReference;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class RunnableServerAvailability extends AsyncTask<String, String, Boolean> {

    private OkHttpClient client;

    private WeakReference<Context> context;


    public RunnableServerAvailability(Context context) {
        super();
        //Initialize OkHttp client
        this.client = HttpOkClientSingleton.getInstance().getClient();
        //Initialize DashboardMapActivity context
        this.context = new WeakReference<>(context);
    }

    @Override
    protected Boolean doInBackground(String... urls) {
        String availabilityUrl = urls[0];
        Request request = new Request.Builder()
                .url(availabilityUrl)
                .build();

        try {
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();
            String apiResponse;
            if (responseBody != null) {
                apiResponse = responseBody.string();
                return Boolean.parseBoolean(apiResponse);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //TODO: IMPLEMENT HERE PRE-PROCESS MESSAGE
    }

    @Override
    protected void onPostExecute(Boolean result) {
        Boolean isOnline = (Boolean)result;
        if (this.isContextAvailable() && isOnline != null) {
            Context context = this.context.get();
            ((DashboardMapActivity)context).handleStatus(isOnline);
        }
    }

    private boolean isContextAvailable()
    {
        Context context = this.context.get();
        return context != null;
    }
}
