package com.gps.dmf.frausto.localizadorgps;

import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Float zoom = 14.5f;
    private Marker marker;
    private Double lat;
    private Double lng;
    private String fecha;
    public String url= "http://gps.frausto.mx/api/logs/last";
    public Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker and move the camera
        LatLng home = new LatLng(20.6244687, -103.4785948);
        marker = mMap.addMarker(new MarkerOptions().position(home).title("Marker in casita"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(home));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(zoom));
        animateMarker();
    }

    public void setMarkerPosition(String markerData, Marker marker) throws JSONException {
        if(markerData.length() > 0 && marker != null) {
            this.setPositionData(markerData);
            LatLng newLocation = new LatLng(this.getLat(), this.getLng());
            marker.setPosition(newLocation);
            marker.setTitle(this.getFecha());
            mMap.moveCamera(CameraUpdateFactory.newLatLng(newLocation));
        }
    }

    public void setPositionData(String dataPosition) throws JSONException {
        JSONObject Jobject = new JSONObject(dataPosition);
        setLat(Jobject.getDouble("lat"));
        setLng(Jobject.getDouble("long"));
        setFecha(Jobject.getString("fecha"));
    }

    //This method is used to move the marker of each car smoothly when there are any updates of their position
    public void animateMarker() {

        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    //Initialize OkHttp client
                    OkHttpClient client = HttpOkClientSingleton.getInstance().getClient();

                    Request request = new Request.Builder()
                            .url(url)
                            .build();

                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            call.cancel();
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {

                            final String myResponse = response.body().string();


                            MapsActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        setMarkerPosition(myResponse, marker);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        }
                    });

                    handler.postDelayed(this, 3000);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
